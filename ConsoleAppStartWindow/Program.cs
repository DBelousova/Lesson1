﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using Model;

namespace ConsoleAppStartWindow
{
    class Program
    {
        static void Main(string[] args)
        {
            DataWorker DataWorker = new DataWorker();
            var Staff = DataWorker.LoadStaff();
            foreach (var i in Staff)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
            DataWorker.Purchase();
            Staff = DataWorker.LoadStaff();
            foreach (var i in Staff)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;

namespace WindowsFormsStartWindow
{
    

    public partial class Form1 : Form
    {

        DataWorker DataWorker = new DataWorker();

        public Form1()
        {
            InitializeComponent();
            button1.Text = "Начислить зарплату сотрудникам";
            button1.Click += new EventHandler(this.Button1_Click);
            listBox1.Items.AddRange(DataWorker.LoadStaff());
          
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DataWorker.Purchase();
            listBox1.Items.Clear();
            listBox1.Items.AddRange(DataWorker.LoadStaff());
        }

       



    }
}

﻿using System;
using Model;
using System.Collections.Generic;

namespace BusinessLogic
{
    public class DataWorker
    {
        public List<Employee> Staff = new List<Employee>();

        public DataWorker()
        {
            FillStaff();
        }

        public void FillStaff()
        {
            var Tom = new Employee();
            Tom.Name = "Tom";
            Tom.Salary = 0;
            var Jerry = new Employee();
            Jerry.Name = "Jerry";
            Jerry.Salary = 0;
            Staff.Add(Tom);
            Staff.Add(Jerry);
        }


        public System.Object[] LoadStaff()
        {
            var temp = new object[Staff.Count];
            for (int i = 0; i < Staff.Count; i++)
            {
                temp[i] = Staff[i].ToString();
            }
            return temp;
        }

        public void Purchase()
        {
            int salary = 10000;
            foreach(var i in Staff)
            {
                i.Salary = salary;
            }
        }

    }
}
